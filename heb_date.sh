#!/bin/bash

# heb_date.sh
# A shell utility to display the full Hebrew date in Herbew letters
# Created by Mike Zahavi -- @montdor

# Requires hebcal, awk, and fribidi to be installed, as well as a font that is able to display Hebrew characters
# I am currently using the 'Alef' font, which is a Monospace Hebrew font


# Set a variable for the gershayim symbol and the geresh symbol so that we do not need to insert Unicode every time

gershayim=$'\u05F4'
geresh=$'\u05F3'

# Get the various variables that are needed to print out

month_name=$(hebcal -T8 |
(awk 'NR==1{gsub (",",""); print $3}'))

day=$(hebcal -T8 |
(awk 'NR==1{print $1}'))

year=$(hebcal -T8 |
(awk 'NR==1{print $4}'))

## Check to see if there is a second or third line (e.g. Holiday) and set it to a variable

second_line=$(hebcal -T8 | (awk 'NR==2'))
third_line=$(hebcal -T8 | (awk 'NR==3')) # Mostly the Omer

## Set number of day from second line into variable

third_line_day=$(hebcal -T8 |
(awk 'NR==3{print $1}'))

third_line_event=$(hebcal -T8 |
(awk 'NR==3{print $5}'))

# Convert the day of the month into the Hebrew letters that equal it

case $day in
    "1st") hebrew_day=א;;
    "2nd") hebrew_day=ב;;
    "3rd") hebrew_day=ג;;
    "4th") hebrew_day=ד;;
    "5th") hebrew_day=ה;;
    "6th") hebrew_day=ו;;
    "7th") hebrew_day=ז;;
    "8th") hebrew_day=ח;;
    "9th") hebrew_day=ט;;
    "10th") hebrew_day=י;;
    "11th") hebrew_day=יא;;
    "12th") hebrew_day=יב;;
    "13th") hebrew_day=יג;;
    "14th") hebrew_day=יד;;
    "15th") hebrew_day=ט$gershayimו;;
    "16th") hebrew_day=ט$gershayimז;;
    "17th") hebrew_day=יז;;
    "18th") hebrew_day=יח;;
    "19th") hebrew_day=יט;;
    "20th") hebrew_day=כ;;
    "21st") hebrew_day=כא;;
    "22nd") hebrew_day=כב;;
    "23rd") hebrew_day=כג;;
    "24th") hebrew_day=כד;;
    "25th") hebrew_day=כה;;
    "26th") hebrew_day=כו;;
    "27th") hebrew_day=כז;;
    "28th") hebrew_day=כח;;
    "29th") hebrew_day=כט;;
    "30th") hebrew_day=ל;;
    "*") hebrew_day=$day;
esac


# Convert the year into the Hebrew letters that equal it

if [ $year == 5779 ]; then hebrew_year=תשע$gershayimט
elif [ $year == 5780 ]; then hebrew_year=תש$gershayimפ
else hebrew_year=$year
fi

case $second_line_day in
    "1st") third_line_hebrew_day=א;;
    "2nd") third_line_hebrew_day=ב;;
    "3rd") third_line_hebrew_day=ג;;
    "4th") third_line_hebrew_day=ד;;
    "5th") third_line_hebrew_day=ה;;
    "6th") third_line_hebrew_day=ו;;
    "7th") third_line_hebrew_day=ז;;
    "8th") third_line_hebrew_day=ח;;
    "9th") third_line_hebrew_day=ט;;
    "10th") third_line_hebrew_day=י;;
    "11th") third_line_hebrew_day=יא;;
    "12th") third_line_hebrew_day=יב;;
    "13th") third_line_hebrew_day=יג;;
    "14th") third_line_hebrew_day=יד;;
    "15th") third_line_hebrew_day=ט$gershayimו;;
    "16th") third_line_hebrew_day=ט$gershayimז;;
    "17th") third_line_hebrew_day=יז;;
    "18th") third_line_hebrew_day=יח;;
    "19th") third_line_hebrew_day=יט;;
    "20th") third_line_hebrew_day=כ;;
    "21st") third_line_hebrew_day=כא;;
    "22nd") third_line_hebrew_day=כב;;
    "23rd") third_line_hebrew_day=כג;;
    "24th") third_line_hebrew_day=כד;;
    "25th") third_line_hebrew_day=כה;;
    "26th") third_line_hebrew_day=כו;;
    "27th") third_line_hebrew_day=כז;;
    "28th") third_line_hebrew_day=כח;;
    "29th") third_line_hebrew_day=כט;;
    "30th") third_line_hebrew_day=ל;;
    "*") third_line_hebrew_day=$second_line_day;
esac

# Check to see what the event is in the second line
case $second_line_event in
    "Omer") second_line_hebrew_event=עומר
esac

# Convert third line to Hebrew letters, as above

case $third_line_day in
    "1st") third_line_hebrew_day=א;;
    "2nd") third_line_hebrew_day=ב;;
    "3rd") third_line_hebrew_day=ג;;
    "4th") third_line_hebrew_day=ד;;
    "5th") third_line_hebrew_day=ה;;
    "6th") third_line_hebrew_day=ו;;
    "7th") third_line_hebrew_day=ז;;
    "8th") third_line_hebrew_day=ח;;
    "9th") third_line_hebrew_day=ט;;
    "10th") third_line_hebrew_day=י;;
    "11th") third_line_hebrew_day=יא;;
    "12th") third_line_hebrew_day=יב;;
    "13th") third_line_hebrew_day=יג;;
    "14th") third_line_hebrew_day=יד;;
    "15th") third_line_hebrew_day=ט$gershayimו;;
    "16th") third_line_hebrew_day=ט$gershayimז;;
    "17th") third_line_hebrew_day=יז;;
    "18th") third_line_hebrew_day=יח;;
    "19th") third_line_hebrew_day=יט;;
    "20th") third_line_hebrew_day=כ;;
    "21st") third_line_hebrew_day=כא;;
    "22nd") third_line_hebrew_day=כב;;
    "23rd") third_line_hebrew_day=כג;;
    "24th") third_line_hebrew_day=כד;;
    "25th") third_line_hebrew_day=כה;;
    "26th") third_line_hebrew_day=כו;;
    "27th") third_line_hebrew_day=כז;;
    "28th") third_line_hebrew_day=כח;;
    "29th") third_line_hebrew_day=כט;;
    "30th") third_line_hebrew_day=ל;;
    "*") third_line_hebrew_day=$third_line_day;
esac

# Check to see what the event is in the second line
case $third_line_event in
    "Omer") third_line_hebrew_event=עומר
esac

# Print the string that we want

echo "$hebrew_day" ב"$month_name", ה$geresh"$hebrew_year" | fribidi --ltr

# if $second_line = '' # if the second line is blank, there is no third, so only print the first
#     then
#         echo "$hebrew_day" ב"$month_name", ה$geresh"$hebrew_year" | fribidi --ltr
# elif $third_line = '' # if the third line is blank, print the first and second
#     then
#         echo "$hebrew_day" ב"$month_name", ה$geresh"$hebrew_year" "($second_line)" | fribidi --ltr
# else # neither the second or third line is blank, print them both out
#     echo "$hebrew_day" ב"$month_name", ה$geresh"$hebrew_year" "($second_line; $third_line_hebrew_day ב$third_line_hebrew_event)" | fribidi --ltr
# fi
